
<?php get_header(); ?>
	<div class="page-title">
		<h1><?php wp_title(''); ?></h1>
		<span class="title-line"></span>
	</div>
</header>
<section id="main-content" class="wrap group">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; else: ?>
			<h2>Resultado</h2>
			<p>Não foram encontrados artigos.</p>
		<?php endif; ?>
	</ul>
</section>

<?php get_footer(); ?>
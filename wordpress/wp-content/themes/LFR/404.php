<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="page-error">
    <div class="container internas">

        <div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">
            <br><br>

                <header class="page-header">
                    <h1 class="title"><?php _e( 'Página não encontrada', 'twentythirteen' ); ?></h1>
                </header>

                <div class="page-wrapper">
                    <div class="page-content">

                        <p>
                            <?php _e( 'A página que você busca não foi encontrada.', 'twentythirteen' ); ?>
                        </p>

                        <?php get_search_form(); ?>
                    </div><!-- .page-content -->
                </div><!-- .page-wrapper -->

            </div><!-- #content -->
        </div><!-- #primary -->

    </div>
</div>
<?php get_footer(); ?>

    <footer class="footer">
        <div class="container">
            <div class=" col-xs-12 col-sm-6 col-md-2 midias-sociais">
                <div class="facebook col-xs-6">
                    <a href="https://www.facebook.com/lfradvocacia" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/img/facebook.svg" class="icon">
                    </a>
                </div>
                <div class="linkedin col-xs-6">
                    <a href="https://www.linkedin.com/company-beta/16238323/?pathWildcard=16238323" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/img/linkedin.svg" class="icon">
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 informacoes">
                <div class="col-a">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
                <div class="col-b">
                    <p>
                        Rua Comendador Araújo, 499 | 10º andar<br>
                        Batel | Curitiba | PR | 80420-000
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 informacoes">
                <div class="col-a">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </div>
                <div class="col-b">
                    <p>+55 41 2106-6746</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 informacoes info-email">
                <div class="col-a">
                    <a href="mailto:contato@lfradvocacia.com.br"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                </div>
                <div class="col-b">
                    <p><a href="mailto:contato@lfradvocacia.com.br">contato@lfradvocacia.com.br</a></p>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
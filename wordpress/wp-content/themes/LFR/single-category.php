<?php get_header(); ?>
<?php
	  // $post = $wp_query->post;

	  // if (in_category('16')) {
	  //     include('single-category.php');
	  // }elseif (in_category('17')) {
	  //     include('single-category.php');
	  // } elseif (in_category('18')) {
	  //     include('single-category.php');
	  // } elseif (in_category('19')) {
	  //     include('single-category.php');
	  // } else {
	  // 		echo "zdfzdfs";
	  // }
?>

<div class="page-posts">
    <div class="img-header">
        <img src="<?php bloginfo('template_url'); ?>/img/img-publicacoes-mini.jpg" class="foto1">
    </div>

    <div class="container internas">

        <div class="page-title">
            <h1><?php the_category(' ') ?></h1>
            <span class="title-line"></span>
        </div>


        <section id="main-content" class="wrap project">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article class="project-content group">
            <div class="project-photos fl">
                <?php the_content()?>
            </div>
            <div class="project-info fr">
                <h2 class="project-title"><?php the_title()?></h2>
                <p><?php the_excerpt()?></p>
                <?php the_meta(); ?>
                <p>
                <div class="project-share"><span class="bold-museo">Share on</span><?php do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'small_toolbox'); ?></div>
                <div class="project-tags"><span class="bold-museo">Tags</span> | <?php the_tags('', ' / ', ''); ?></div>
                <p class="view-info">Clique nas imagens para ampliar<br /><em>Click images to enlarge</em></p>
            </div>
        </article>
        <div class="pagination single-post-link">
            <div class="back-to fl"><a href="">Works</a></div>
            <div class="see-projects fr">
                <span class="prev fl"><?php next_post_link('%link', '%title', true); ?></span>
                <span class="next fl"><?php previous_post_link('%link', '%title', true); ?></span>
            </div>
        </div>

        <?php endwhile; else: ?>

            <h2>Resultado</h2>

            <p>Não foram encontrados artigos.</p>

        <?php endif; ?>

        </section>

    </div>

    <div class="border-orange-bottom"></div>

</div>
<?php get_footer(); ?>
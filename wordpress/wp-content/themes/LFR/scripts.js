$(document).ready(function(){
	submenu();
	searchEffect();
	inputLabel();
	classOpacity();
	// 	$('.toggle-menu').on('click',function(e){
	//     e.preventDefault();
	//     $(this).parent().siblings().children('.toggle-menu').removeClass('show').next().slideUp();
	//     $(this).toggleClass('show').next().slideToggle();
	// });
});

var submenu = function () {

	var menuWork = $("#menu-item-110")
	, submenuWork = $("#menu-item-110").find('.sub-menu');
	
	menuWork.hover(
		function () {
		submenuWork.fadeIn(1200, 'easeOutExpo');
		},
		function () {
		submenuWork.css({display: 'none'});
		}
	);	
}

var searchEffect = function () {

	var search = $('#s');

	search.focus(function(){
		$(this).animate({width: '186px'}, 700, 'easeOutExpo');
	});
	search.focusout(function(){
		$(this).animate({width: '100px'}, 700, 'easeOutExpo');
	}); 
}

var inputLabel = function () {

	$('.wpcf7-text, .message textarea').focus(function() {
		valInput = $(this).val()
		, attrName = $(this).attr("name"); 

		if (valInput.toLowerCase()==attrName.toLowerCase()) {
			$(this).val("");
		}
	}).blur(function() {
		if (valInput=="") { $(this).val(attrName.substr(0, 1).toUpperCase() + attrName.substr(1)); }
	});
}

var classOpacity = function() {

	var seeProjects = $('.see-projects').find('span');

	seeProjects.each(function(){
		if (!$(this).find('a').length) {
			$(this).addClass('removePagSt');
		}
	})

}
<?php get_header(); ?>




<main class="main">

        <section id="intro">
            <div class="container">

                <div class="logo-big">
                    <img src="<?php bloginfo('template_url'); ?>/img/logo-big2.png" class="img-responsive" alt="Logo da Luvisotto Franz & Romano advogados">
                </div>
                <div class="text">
                    <p>Com a atuação direta dos três sócios fundadores, a LFR Advocacia auxilia legalmente seus clientes, pessoas jurídicas e físicas, de forma proativa, visando a otimização da carga tributária e o apoio nas questões estratégicas e confidenciais, proporcionando uma profunda visão de todas as alternativas, benefícios e eventuais riscos envolvidos em cada cenário.</p>
                </div>
            </div>
        </section>

        <section id="quem-somos">
            <div class="container">

                <h2 class="title">Quem Somos</h2>

                <div class="row">

                    <div class="guilherme col-sm-12 col-lg-4 col-md-6">
                        <img src="<?php bloginfo('template_url'); ?>/img/guilherme.jpg" class="foto1 img-responsive" alt="Guilherme Augusto Luvisotto">

                        <h2 class="tit">Guilherme Augusto Luvisotto</h2>
                        <h5 class="sub-tit">OAB/PR 43.626</h5>
                        <p>Advogado formado pela Faculdade de Direito de Curitiba e Contador pela Pontifícia Universidade Católica do Paraná, com mais de 10 anos de experiência. Atuou em grande empresa internacional de auditoria e em escritórios de advocacia de grande porte. Consultor tributário com especialização em direito e processo tributário. Professor convidado de cursos de pós graduação em “Gestão Tributária”.</p>
                    </div>

                    <div class="guilherme col-sm-12 col-lg-4 col-md-6">
                        <img src="<?php bloginfo('template_url'); ?>/img/Luiz-Eduardo-Franz.jpg" class="foto1 img-responsive" alt="Luiz Eduardo Franz">

                        <h2 class="tit">Luiz Eduardo Franz</h2>
                        <h5 class="sub-tit">OAB/PR 46.478</h5>
                        <p>Advogado formado pela Faculdade de Direito de Curitiba. Pós- graduado (MBA) em Gestão Empresarial pela ISAE-FGV/PR e especializado em Direito Societário pela Academia Brasileira de Direito Constitucional. Possui mais de 10 anos de experiência como gerente sênior de consultoria tributária em uma das maiores firmas de auditoria do mundo.</p>
                    </div>

                    <div class="guilherme col-sm-12 col-lg-4 col-md-6">
                        <img src="<?php bloginfo('template_url'); ?>/img/Paulo-Eduardo-Romano.jpg" class="foto1 img-responsive" alt="Paulo Eduardo Romano">

                        <h2 class="tit">Paulo Eduardo Romano</h2>
                        <h5 class="sub-tit">OAB/PR 45.628</h5>
                        <p>Advogado formado em Direito pela Pontifícia Universidade Católica do Paraná e em Ciências Contábeis pela Universidade do Sul de Santa Catarina. Pós- graduado (LL.M.) em Direito Tributário e Contabilidade.Tributária pelo IBMEC/RJ. Com mais de 10 anos de experiência atuando como consultor tributário em uma das maiores firmas de auditoria do mundo e como advogado sênior em escritório de advocacia de ponta.</p>
                    </div>

                </div>

            </div>

        </section>





        <section id="areas-de-atuacao">
            <div class="container">

                <h2 class="title">Áreas de atuação</h2>

                <div class="row">
                    <div class="col-sm-12">
                        <h3>Consultoria Tributária</h3>
                        <p>
                            A experiência dos profissionais da LFR Advocacia é primordial na análise e obtenção dos resultados apresentados e delineados aos clientes. A área de consultoria tributária elabora pareceres jurídicos, opiniões legais e respostas objetivas às questões levantadas pelos clientes, atendendo de forma personalizada e solucionando a demanda gerada. A consultoria abrange temas relacionados a qualquer tributo, sempre considerando os aspectos específicos do segmento de atuação do cliente.
                        </p>
                        <p>Entre outros serviços, os profissionais estão preparados para oferecer:</p>
                        <ul>
                            <li>Revisão das bases de cálculo do IRPJ e CSLL;</li>
                            <li>Revisão dos procedimentos adotados em relação à apuração e recolhimento das contribuições do PIS e da COFINS;</li>
                            <li>Revisão dos procedimentos utilizados em relação à apuração e recolhimento do ICMS e IPI;</li>
                            <li>Revisão das obrigações acessórias exigidas pela Receita Federal do Brasil;</li>
                            <li>Diagnóstico Fiscal, identificação de oportunidades de redução da carga tributária global da empresa a partir da reorganização societária ou estruturação de operações;</li>
                            <li>Gestão tributária estratégica preventiva através de consultoria permanente;</li>
                            <li>Elaboração de pareceres jurídicos, opiniões legais e cartas consultas.</li>
                        </ul>

                        <div class="margin30"></div>

                        <h3> Contencioso Tributário (Administrativo e Judicial)</h3>

                        <p>Contando com a experiência dos profissionais tanto em Direito Tributário, quanto em Contabilidade, a área de contencioso da LFR Advocacia destaca-se na elaboração de teses personalizadas para defesa dos interesses dos clientes, visando:</p>

                        <div class="margin30"></div>

                        <ul>
                            <li>Defesa de Autos de Infração nos âmbitos Administrativo e Judicial;</li>
                            <li>Elaboração de teses para redução da carga tributária;</li>
                            <li>Orientação e revisão para a formação do conjunto probatório.</li>
                        </ul>


                        <br>


                        <h3> Planejamento Sucessório e Proteção Patrimonial</h3>
                        <p> Com diagnóstico personalizado das demandas e objetivos da família, a área de planejamento sucessório busca solução em reorganizações societárias, elaboração de<br> acordos de acionistas e protocolos familiares, tributação de renda e herança, constituição de fundos.</p>

                        <br>

                        <h3>Direito Societário / Empresarial</h3>
                        <p>Com reflexo direto decorrentes de eventuais alterações tributárias, a equipe da LFR Advocacia possui experiência para a análise e preparação de soluções artesanais às demandas de seus clientes, envolvendo reestruturação da Sociedade, alterações de contrato/ estatuto social, bem como assessoria em processos de aquisições e fusões (M&A), sob o viés tributário.</p>
                    </div>
                </div>

            </div>
        </section>

        <section id="publicacoes">
            <div class="container">

                <h2 class="title">Publicações</h2>

                     <?php
                        $args = array( 'numberposts' => 6, 'post_status'=>"publish",'post_type'=>"post",'orderby'=>"post_date");
                        $postslist = get_posts( $args );
                        echo '<div class="row">';

                            foreach ($postslist as $post) :  setup_postdata($post); ?>

                                <div class="col-lg-4 col-md-6">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                    <div class="title-section"><?php the_category(); ?></div>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
                                        <h2><?php the_title(); ?></h2>
                                        <p>
                                            <?php
                                                $excerpt = get_the_excerpt();
                                                $excerpt = substr( $excerpt , 0, 500);
                                                echo $excerpt;
                                            ?>
                                            ...
                                        </p>
                                    </a>
                                </div>

                            <?php endforeach; ?>

            </div>
        </section>

        <section id="contato">
            <div class="container ">

                <h2 class="title">Contato</h2>
                <div class="clearfix">
                    <div class="col-md-6 col-xs-12">
                        <div class="google-maps">
                            <div id="map"></div>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-12">
                        <?php echo do_shortcode( '[contact-form-7 id="6" title="Contato (home)"]' ); ?>
                    </div>
                </div>

            </div>
        </section>

    </main>


<script>

function initMap() {
    var myLatLng = {lat: -25.4370785, lng: -49.2820981};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: myLatLng,
      scrollwheel: false,
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'LFR - Advocacia'
    });
  }



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxTGAC_nLETHH0doAZQL0pX6cfrKzxJnw&callback=initMap"
    async defer></script>


<?php get_footer(); ?>
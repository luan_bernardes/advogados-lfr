<?php get_header(); ?>

<div class="page-posts">
    <div class="img-header">
        <img src="<?php bloginfo('template_url'); ?>/img/img-publicacoes-mini.jpg" class="foto1">
    </div>

    <div class="container internas">

        <h2 class="title">
            Categoria:
            <span class="category">
                <?php
                $category = get_the_category();
                echo $category[0]->cat_name;
                ?>
            </span>
        </h2>

        <section class="list">

            <ul class="posts-list">

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <li>

                    <a href="<?php the_permalink() ?>" title="">
                        <h2 class="sub-title"><?php the_title(); ?></h2>
                        <div class="text">
                        <?php
                            $excerpt = get_the_excerpt();
                            $excerpt = substr( $excerpt , 0, 500);
                            echo $excerpt;
                        ?>
                        ...
                        </div>
                    </a>

                </li>
                <?php endwhile; else: ?>
                <li>
                    <h2>Resultado</h2>
                    <p>Não foram encontrados categorias.</p>
                </li>
                <?php endif; ?>
            </ul>

        </section>

    </div>

    <div class="border-orange-bottom"></div>
</div>

<?php get_footer(); ?>
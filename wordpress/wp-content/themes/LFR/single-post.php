<?php get_header(); ?>

<div class="page-posts">

    <div class="img-header">
        <img src="<?php bloginfo('template_url'); ?>/img/img-publicacoes-mini-2.jpg" class="foto1">
    </div>

    <div class="container internas">

        <section class="post">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <article>

                <h1 class="title"><?php the_title()?></h1>
                <h2 class="category"><?php the_category(' ') ?></h2>
                <div><?php the_content()?></div>



            </article>


            <?php endwhile; else: ?>

                <h2>Resultado</h2>
                <p>Não foram encontrados artigos.</p>

            <?php endif; ?>

        </section>

    </div>

    <div class="border-orange-bottom"></div>

</div>
<?php get_footer(); ?>
(function(){

    var headerHeight = function(){

        var windowWidth = $(window).width();

        //add quando for meio termo, para n quebrar o menu
        if(windowWidth <= 992 && windowWidth >= 768){
            $('.header').addClass('header-scroll');
        } else {
            $('.header').removeClass('header-scroll');
        }

        //remover qnd for pequeno
        if(windowWidth <= 768){
            $('.header').removeClass('header-scroll');
        }

    };
    headerHeight();


    $(document).scroll(function () {

        var scrollPx = $(document).scrollTop(),
            windowWidth = $(window).width();

        if(scrollPx >= 75 && windowWidth >= 768){
            $('.header').addClass('header-scroll');
            //$('.footer').addClass('footer-scroll');
        } else {
            $('.header').removeClass('header-scroll');
            //$('.footer').removeClass('footer-scroll');
        }

        headerHeight();

    });

    $(document).on('click','.hamburguer', function () {

        if( ! $(this).hasClass('active') ) {
            $(this).addClass('active');
            $('.header .menu').fadeIn();
        } else {
            $(this).removeClass('active');
            $('.header .menu').fadeOut();
        }

    });

    $(document).on('click','.header .menu', function () {
        //remover qnd for pequeno
        console.log('aaa')
        if(windowWidth <= 768){
            $('.header .hamburguer').click()
        }
    });

    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 60
                    }, 1000);
                    $('.header .hamburguer').click()
                    return false;
                }
            }
        });
    });


})();


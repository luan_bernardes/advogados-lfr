<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="description" content="">
    <meta property="og:description" content="" />

    <title>LFR - Advocacia</title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/vendors/bootstrap/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>

    <!-- fonts-->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/fonts/MyFontsWebfontsKit.css"/>

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">

</head>
<body>

    <header class="header">
        <div class="container">
            <div class="logo-topo col-xs-6 col-sm-2">
                <div class="row">
                    <a href="/">
                        <img src="<?php bloginfo('template_url'); ?>/img/luvisotto-03.svg" width="100%" alt="LFR Advocacia logo">
                    </a>
                </div>
            </div>
            <button type="button" class="hamburguer">
                <span class="sr-only">Alternar de navegação</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="col-xs-12 col-sm-10">

                <ul class="menu">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/#quem-somos">Quem somos</a>
                    </li>
                    <li>
                        <a href="/#areas-de-atuacao">Áreas de atuação</a>
                    </li>
                    <li>
                        <a href="/#publicacoes">Publicações</a>
                    </li>
                    <li>
                        <a href="/#contato">Contato</a>
                    </li>
                    <!-- <li>
                    <!-- <li>
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </li> -->
                </ul>

            </div>
        </div>
    </header>

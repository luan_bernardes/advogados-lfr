<?php
$args = array(
    'id_form' => 'commentform',
    'id_submit' => 'submit',
    'title_reply' => __( 'Leave a reply' ),
    'title_reply_to' => __( 'Leave a reply for %s' ),
    'cancel_reply_link' => __( 'Cancel comment' ),
    'label_submit' => __( 'Comment' ),
    'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( '', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
    'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'Você precisa estar<a href="%s">  logado </a> para comentar.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
    'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logado como <a href="%1$s">%2$s</a>. <a href="%3$s" title="Logout">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
    'comment_notes_before' => '<p class="comment-notes">' . __( '' ) . ( $req ? $required_text : '' ) . '</p>',
    'comment_notes_after' => '<p class="form-allowed-tags">' . sprintf( __( '' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
    'fields' => apply_filters( 'comment_form_default_fields', array(
        'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Nome', 'domainreference' ) . '</label> ' . ( $req ? '<span class="required"></span>' : '' ) . '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
        'email' => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'domainreference' ) . '</label> ' . ( $req ? '<span class="required"></span>' : '' ) . '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>') ) );
?>
<?php comment_form( $args, $post_id ); ?>
<?php
if (class_exists('Walker_Comment_Wink'))
  $walker = new Walker_Comment_Wink();
else
  $walker = '';
 
wp_list_comments(array('walker' => $walker));
?>
<div class="commentPagination">
<?php paginate_comments_links(); ?> 
</div>

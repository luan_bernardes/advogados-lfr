<?php get_header(); ?>

<div class="container internas">

	<div class="page-title">
		<h1>Search</h1>
		<span class="title-line"></span>
	</div>



	<ul class="post-archive fr">
	</ul>
	<h2 class="search-heading">Results for: <?php /* Search Count */ $allsearch = &new WP_Query("s=$s&showposts=-1"); $key = wp_specialchars($s, 1); $count = $allsearch->post_count; _e(''); _e('<span class="search-terms">'); echo $key; _e('</span>'); _e(' — '); echo $count . ' '; _e('articles'); wp_reset_query(); ?></h2>
	<br />
	<hr />
	<br />
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article class="post-content group fl">
			<div class="post-info">
				<h2 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title()?></a></h2>
			</div>
				<?php the_content()?>
		</article>
	<?php endwhile; ?>
<!-- 		<article class="post-content group fl">
			<h2>Resultado</h2>
			<p>Não foram encontrados artigos.</p>
		</article> -->
	<?php endif; ?>

	<div class="pagination">
		<div class="back-to fl"><a href="<?php echo home_url(); ?>">Works</a></div>
		<div class="see-projects fr">
			<span class="prev fl"><?php previous_posts_link( '' , 0 ); ?></span>
			<span class="next fl"><?php next_posts_link( '' , 0 ); ?></span>
		</div>
	</div>

</div>

<?php get_footer(); ?>